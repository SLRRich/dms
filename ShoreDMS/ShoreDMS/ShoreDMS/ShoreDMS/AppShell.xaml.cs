﻿using ShoreDMS.ViewModels;
using ShoreDMS.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ShoreDMS
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(DocumentTypeDetailPage), typeof(DocumentTypeDetailPage));
            Routing.RegisterRoute(nameof(NewDocumentTypePage), typeof(NewDocumentTypePage));
            Routing.RegisterRoute(nameof(NewDocumentPage), typeof(NewDocumentPage));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}
