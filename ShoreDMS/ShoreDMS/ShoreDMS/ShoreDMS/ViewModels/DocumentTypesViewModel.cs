﻿using ShoreDMS.Models;
using ShoreDMS.Views;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using DMSService;
using System.ServiceModel;

namespace ShoreDMS.ViewModels
{
    public class DocumentTypesViewModel : BaseViewModel
    {
        private DocumentType _selectedItem;

        public ObservableCollection<DocumentType> DocumentTypes { get; }
        public Command LoadDocumentTypesCommand { get; }
        public Command AddDocumentTypeCommand { get; }
        public Command<DocumentType> DocumentTypeTapped { get; }

        public DocumentTypesViewModel()
        {
            Title = "Document Types";
            DocumentTypes = new ObservableCollection<DocumentType>();
            LoadDocumentTypesCommand = new Command(async () => await ExecuteLoadDocumentTypesCommand());

            DocumentTypeTapped = new Command<DocumentType>(OnDocumentTypeSelected);

            AddDocumentTypeCommand = new Command(OnAddDocumentType);
        }

        async Task ExecuteLoadDocumentTypesCommand()
        {
            IsBusy = true;

            try
            {
                DocumentTypes.Clear();
                
                HttpBindingBase binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
                EndpointAddress endpoint = new EndpointAddress(EndpointUri);

                DocumentServiceClient client = new DocumentServiceClient(binding, endpoint);

                GetDocumentTypesRequest request = new GetDocumentTypesRequest();
                GetDocumentTypesResponse response = await client.GetDocumentTypesAsync(request);

                var documentTypes = response.GetDocumentTypesResult;
                foreach (var item in documentTypes)
                {
                    DocumentTypes.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public DocumentType SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnDocumentTypeSelected(value);
            }
        }

        private async void OnAddDocumentType(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewDocumentTypePage));
        }

        async void OnDocumentTypeSelected(DocumentType item)
        {
            if (item == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(DocumentTypeDetailPage)}?{nameof(DocumentTypeDetailViewModel.DocumentTypeID)}={item.ID}");
        }
    }
}