﻿using ShoreDMS.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using DMSService;
using System.ServiceModel;

namespace ShoreDMS.ViewModels
{
    public class NewDocumentViewModel : BaseViewModel
    {
        private string name;
        private int documentTypeID;

        public NewDocumentViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(name);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public int DocumentTypeID
        {
            get => documentTypeID;
            set => SetProperty(ref documentTypeID, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            string filename = "";
            string documentSourceCode = "";
            byte[] filedata = new byte[1];
                
            HttpBindingBase binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            EndpointAddress endpoint = new EndpointAddress(EndpointUri);

            DocumentServiceClient client = new DocumentServiceClient(binding, endpoint);

            CreateDocumentRequest request = new CreateDocumentRequest(
                name,
                documentTypeID,
                filename,
                documentSourceCode,
                filedata);
            CreateDocumentResponse response = await client.CreateDocumentAsync(request);


            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
