﻿using ShoreDMS.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using DMSService;
using System.ServiceModel;

namespace ShoreDMS.ViewModels
{
    public class NewDocumentTypeViewModel : BaseViewModel
    {
        private string name;
        private bool isReadOnly;

        public NewDocumentTypeViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(name);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public bool IsReadOnly
        {
            get => isReadOnly;
            set => SetProperty(ref isReadOnly, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            DocumentType newItem = new DocumentType()
            {
                Name = Name,
                IsReadOnly = IsReadOnly
            };

            HttpBindingBase binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            EndpointAddress endpoint = new EndpointAddress(EndpointUri);

            DocumentServiceClient client = new DocumentServiceClient(binding, endpoint);

            SetDocumentTypeRequest request = new SetDocumentTypeRequest(newItem);
            SetDocumentTypeResponse response = await client.SetDocumentTypeAsync(request);


            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
