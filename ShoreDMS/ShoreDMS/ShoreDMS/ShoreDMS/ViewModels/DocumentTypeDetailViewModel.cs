﻿using ShoreDMS.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using DMSService;
using System.ServiceModel;

namespace ShoreDMS.ViewModels
{
    [QueryProperty(nameof(DocumentTypeID), nameof(DocumentTypeID))]
    public class DocumentTypeDetailViewModel : BaseViewModel
    {
        private int documentTypeID;
        private string name;
        private bool isReadOnly;
        public int Id { get; set; }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public bool IsReadOnly
        {
            get => isReadOnly;
            set => SetProperty(ref isReadOnly, value);
        }

        public int DocumentTypeID
        {
            get
            {
                return documentTypeID;
            }
            set
            {
                documentTypeID = value;
                LoadDocumentTypeID(value);
            }
        }

        public async void LoadDocumentTypeID(int documentTypeID)
        {
            try
            {
                HttpBindingBase binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
                EndpointAddress endpoint = new EndpointAddress(EndpointUri);

                DocumentServiceClient client = new DocumentServiceClient(binding, endpoint);

                GetDocumentTypeRequest request = new GetDocumentTypeRequest(documentTypeID);
                GetDocumentTypeResponse response = await client.GetDocumentTypeAsync(request);

                var documentType = response.GetDocumentTypeResult;

               
                Id = documentType.ID;
                Name = documentType.Name;
                IsReadOnly = documentType.IsReadOnly;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Document Type");
            }
        }
    }
}
