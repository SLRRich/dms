﻿using ShoreDMS.Models;
using ShoreDMS.Views;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using DMSService;
using System.ServiceModel;

namespace ShoreDMS.ViewModels
{
    public class DocumentsViewModel : BaseViewModel
    {
        //private Document _selectedItem;

        public ObservableCollection<Document> Documents { get; }
        public Command LoadDocumentsCommand { get; }
        public Command AddDocumentCommand { get; }
        public Command<Document> DocumentTapped { get; }

        public DocumentsViewModel()
        {
            Title = "Documents";
            Documents = new ObservableCollection<Document>();
            LoadDocumentsCommand = new Command(async () => await ExecuteLoadDocumentsCommand());

            //DocumentTapped = new Command<Document>(OnDocumentSelected);

            AddDocumentCommand = new Command(OnAddDocument);
        }

        async Task ExecuteLoadDocumentsCommand()
        {
            IsBusy = true;

            try
            {
                Documents.Clear();

                HttpBindingBase binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
                EndpointAddress endpoint = new EndpointAddress(EndpointUri);

                DocumentServiceClient client = new DocumentServiceClient(binding, endpoint);

                GetDocumentsRequest request = new GetDocumentsRequest();
                GetDocumentsResponse response = await client.GetDocumentsAsync(request);

                var documents = response.GetDocumentsResult;
                foreach (var item in documents)
                {
                    Documents.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            // SelectedItem = null;
        }

        private async void OnAddDocument(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewDocumentPage));
        }

    }
}