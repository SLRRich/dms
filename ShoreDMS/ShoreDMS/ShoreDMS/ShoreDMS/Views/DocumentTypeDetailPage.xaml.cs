﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ShoreDMS.ViewModels;

namespace ShoreDMS.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentTypeDetailPage : ContentPage
    {
        public DocumentTypeDetailPage()
        {
            InitializeComponent();
            BindingContext = new DocumentTypeDetailViewModel();
        }
    }
}