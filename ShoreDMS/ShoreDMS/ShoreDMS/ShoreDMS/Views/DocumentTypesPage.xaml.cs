﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ShoreDMS.ViewModels;

namespace ShoreDMS.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentTypesPage : ContentPage
    {
        DocumentTypesViewModel _viewModel;

        public DocumentTypesPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new DocumentTypesViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
