﻿using ShoreDMS.Models;
using ShoreDMS.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DMSService;

namespace ShoreDMS.Views
{
    public partial class NewDocumentPage : ContentPage
    {
        public DocumentType Item { get; set; }

        public NewDocumentPage()
        {
            InitializeComponent();
            BindingContext = new NewDocumentViewModel();
        }
    }
}