﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DocumentManagementService
{
    public class DocumentService : IDocumentService
    {
        public string Version ()
        {
            return string.Format("Hello world.");
        }
        public List<DocumentType> GetDocumentTypes()
        {
            var ctx = new DMSDatabaseConnectionString();
            ctx.Configuration.ProxyCreationEnabled = false;
            List<DocumentType> docTypes = ctx.DocumentTypes.ToList();

            return docTypes;
        }
        public DocumentType GetDocumentType(int DocumentTypeID)
        {
            var ctx = new DMSDatabaseConnectionString();
            ctx.Configuration.ProxyCreationEnabled = false;
            DocumentType docType = ctx.DocumentTypes.Where(
                DocumentType => DocumentType.ID == DocumentTypeID).FirstOrDefault();

            return docType;
        }
        public DocumentType SetDocumentType(DocumentType documentType)
        {
            var ctx = new DMSDatabaseConnectionString();
            ctx.Configuration.ProxyCreationEnabled = false;

            if (documentType.ID == 0)
            {
                ctx.DocumentTypes.Add(documentType);
                ctx.SaveChanges();
            }
            else
            {
                DocumentType docType = ctx.DocumentTypes.Where(
                DocumentType => DocumentType.ID == documentType.ID).First();

                docType.Name = documentType.Name;
                docType.IsReadOnly = documentType.IsReadOnly;
                ctx.SaveChanges();

                documentType.ID = docType.ID;
            }

            return documentType;
        }
        public List<DocumentTagType> GetDocumentTagTypes()
        {
            var ctx = new DMSDatabaseConnectionString();
            var docTagTypes = ctx.DocumentTagTypes.ToList();

            return docTagTypes;
        }
        public DocumentTagType SetDocumentTagType()
        {
            var ctx = new DMSDatabaseConnectionString();
            var docTagType = ctx.DocumentTagTypes.Find(1);

            return docTagType;
        }
        public List<DocumentTypeTagType> GetDocumentTypeTagTypes()
        {
            var ctx = new DMSDatabaseConnectionString();
            var docTypeTagTypes = ctx.DocumentTypeTagTypes.ToList();

            return docTypeTagTypes;
        }
        public DocumentTypeTagType SetDocumentTypeTagType()
        {
            var ctx = new DMSDatabaseConnectionString();
            var docTypeTagType = ctx.DocumentTypeTagTypes.Find(1);

            return docTypeTagType;
        }
        public List<Document> GetDocuments ()
        {
            var ctx = new DMSDatabaseConnectionString();
            var docs = ctx.Documents.ToList();
           
            return docs;
        }
        public Document CreateDocument(
            string Name, 
            int DocumentTypeID, 
            string Filename, 
            string DocumentSourceCode,
            byte[] Filedate
            )
        {
            var ctx = new DMSDatabaseConnectionString();
            System.Data.Entity.Core.Objects.ObjectParameter docID = new System.Data.Entity.Core.Objects.ObjectParameter("DocumentID", typeof(Int64)); ;
            System.Data.Entity.Core.Objects.ObjectParameter docVersionID = new System.Data.Entity.Core.Objects.ObjectParameter("DocumentVersionID", typeof(Int64)); ;

            // TODO: Generate the hash of the file we store. 
            var Hash = "";

            ctx.CreateDocument(
                Name,
                DocumentTypeID, 
                Filename,
                Hash,
                DocumentSourceCode,
                docID,
                docVersionID);

            var doc = ctx.Documents.Find(docID);

            return doc;
        }
        public Document CreateDocumentVersion (
            long DocumentID, 
            string Filename, 
            string DocumentSourceCode,
            byte[] Filedata)
        {
            var ctx = new DMSDatabaseConnectionString();
            System.Data.Entity.Core.Objects.ObjectParameter docVersionID = new System.Data.Entity.Core.Objects.ObjectParameter("DocumentVersionID", typeof(Int64)); ;

            // TODO: Generate the hash of the file we store. 
            var Hash = "";

            ctx.CreateDocumentVersion(DocumentID, Filename, Hash, DocumentSourceCode, docVersionID);

            var doc = ctx.Documents.Find(docVersionID);

            return doc;
        }

 
    }
}
