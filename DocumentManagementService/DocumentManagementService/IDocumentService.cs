﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace DocumentManagementService
{
    [ServiceContract]
    interface IDocumentService
    {
        [OperationContract]

        [WebInvoke(Method = "GET",

            ResponseFormat = WebMessageFormat.Json,

            BodyStyle = WebMessageBodyStyle.Wrapped,

            UriTemplate = "GetVersion")]
        string Version();

        [OperationContract]
        [ServiceKnownType(typeof(List<DocumentType>))]
        [ServiceKnownType(typeof(List<Document>))]
        [ServiceKnownType(typeof(List<DocumentTypeTagType>))]
        [WebInvoke(Method = "GET",

           ResponseFormat = WebMessageFormat.Json,

           BodyStyle = WebMessageBodyStyle.Wrapped,

           UriTemplate = "GetDocumentTypes")]
        List<DocumentType> GetDocumentTypes();

        [OperationContract]
        [ServiceKnownType(typeof(List<DocumentType>))]
        [ServiceKnownType(typeof(List<Document>))]
        [ServiceKnownType(typeof(List<DocumentTypeTagType>))]
        [WebInvoke(Method = "GET",

           ResponseFormat = WebMessageFormat.Json,

           BodyStyle = WebMessageBodyStyle.Wrapped,

           UriTemplate = "GetDocumentType")]
        DocumentType GetDocumentType(int ID);

        [OperationContract]
        [ServiceKnownType(typeof(List<DocumentType>))]
        [ServiceKnownType(typeof(List<Document>))]
        [ServiceKnownType(typeof(List<DocumentTypeTagType>))]
        [WebInvoke(Method = "PUT",

           ResponseFormat = WebMessageFormat.Json,

           BodyStyle = WebMessageBodyStyle.Wrapped,

           UriTemplate = "SetDocumentType")]
        DocumentType SetDocumentType(DocumentType documentType);

        [OperationContract]

        [WebInvoke(Method = "GET",

           ResponseFormat = WebMessageFormat.Json,

           BodyStyle = WebMessageBodyStyle.Wrapped,

           UriTemplate = "GetDocumentTagTypes")]
        List<DocumentTagType> GetDocumentTagTypes();

        [OperationContract]

        [WebInvoke(Method = "PUT",

          ResponseFormat = WebMessageFormat.Json,

          BodyStyle = WebMessageBodyStyle.Wrapped,

          UriTemplate = "SetDocumentTagType")]
        DocumentTagType SetDocumentTagType();

        [OperationContract]

        [WebInvoke(Method = "GET",

          ResponseFormat = WebMessageFormat.Json,

          BodyStyle = WebMessageBodyStyle.Wrapped,

          UriTemplate = "GetDocumentTypeTagTypes")]
        List<DocumentTypeTagType> GetDocumentTypeTagTypes();

        [OperationContract]

        [WebInvoke(Method = "PUT",

         ResponseFormat = WebMessageFormat.Json,

         BodyStyle = WebMessageBodyStyle.Wrapped,

         UriTemplate = "SetDocumentTypeTagTypes")]
        DocumentTypeTagType SetDocumentTypeTagType();

        [OperationContract]
        [ServiceKnownType(typeof(List<DocumentType>))]
        [ServiceKnownType(typeof(List<Document>))]
        [WebInvoke(Method = "GET",

            ResponseFormat = WebMessageFormat.Json,

            BodyStyle = WebMessageBodyStyle.Wrapped,

            UriTemplate = "GetDocuments")]
        List<Document> GetDocuments();

        [OperationContract]

        [WebInvoke(Method = "PUT",

            ResponseFormat = WebMessageFormat.Json,

            BodyStyle = WebMessageBodyStyle.Wrapped,

            UriTemplate = "CreateDocument")]
        Document CreateDocument(string Name, int DocumentTypeID, string Filename, string DocumentSourceCode, byte[] Filedata);

        [OperationContract]

        [WebInvoke(Method = "PUT",

            ResponseFormat = WebMessageFormat.Json,

            BodyStyle = WebMessageBodyStyle.Wrapped,

            UriTemplate = "CreateDocumentVersion")]
        Document CreateDocumentVersion(long DocumentID, string Filename, string DocumentSourceCode, byte[] Filedate);
    }
}
