//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DocumentManagementService
{
    using System;
    using System.Collections.Generic;
    
    using System.Runtime.Serialization;
    
    public partial class DocumentTypeTagType
    {
        public int ID { get; set; }
        public int DocumentTypeID { get; set; }
        public int DocumentTagTypeID { get; set; }
        public bool IsCompulsory { get; set; }
        public string ValidationRegEx { get; set; }
        public bool IsImmutable { get; set; }
        public byte SortOrder { get; set; }
    
        public virtual DocumentTagType DocumentTagType { get; set; }
        public virtual DocumentType DocumentType { get; set; }
    }
}
